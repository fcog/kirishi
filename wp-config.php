<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dialogi_kirishi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tpP&Zgz7lfv*$XF2oy59=0APRLkSs&fNmi4WI0IFs0*Gmv1I2;uRE;oHkiDS3Z)S');
define('SECURE_AUTH_KEY',  '0uTY051Kz5F}L=KOH(rmFC<:e4K0PBXkU>,ulKc+uU||]t`PV~liXB;rd9H[~[Bl');
define('LOGGED_IN_KEY',    '/k`[4oUXq5hwU.W>}vMn<6]]I@0R$p9rFcO%zZGG^f7@I(=KOmlg4,5IZyZGu6n(');
define('NONCE_KEY',        'Qv[Pwb/55er12ar:pUUMNib*0cHh71P_PSW$GYS[&(K !Q,j:$?k{f6ZPu)bd03r');
define('AUTH_SALT',        '1rDBe0CxGwhU5]Z8{2GR]H-`! F]c~W):v!LH1X)8pCt`y9Cyd^LLGto8$Fa3=]l');
define('SECURE_AUTH_SALT', '+#w1MlXPh}99(&ByP}kRTQn5hR5,/en#7&ZWFe?0$|K?{NNH$kiiRCrFd46A.~0B');
define('LOGGED_IN_SALT',   'Im*%K@iaf{d[HsKMelzlUbm)*N %6 pYOwd!M_/(S52h:1_xOTR></hZ b8s/?:j');
define('NONCE_SALT',       'bWn)X8F{ {I U36yL&oZeDM>/F,YqYfU6hd;NFt<tb>t~zf2aL-5jY(Z>f8%tIe{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
