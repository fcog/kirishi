<?php
/*
Template Name: Blog
*/
?>
<?php

get_header(); ?>

	<section id="primary" class="content-area inner">

        <div id="content" class="site-content" role="main">


			<header class="entry-header">
					<h1>Blog</h1>
			</header><!-- .archive-header -->

			<div id="content-category">
	        <?php
	        $cur_cat = get_cat_ID( single_cat_title("",false) );

	        $args = array('posts_per_page' => 15,'post_type' => 'post', 'cat' => $cur_cat, 'paged' => get_query_var('paged'));

	        $posts_array = get_posts( $args );

	        foreach ( $posts_array as $post ):
	        ?>   
	    		<article>
		    		<header>
			    		<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>						
					</header>
					<div class="entry-content">
                        <?php if (has_post_thumbnail()): ?>
                            <div class="col span_1_5">
                            <?php the_post_thumbnail('news-thumbnails'); ?>
                            </div>
                            <aside class="col span_4_5">
                        <?php else: ?>
                            <aside>
                        <?php endif ?>
                        <p class="entry-date">
						<?php _e('Posted') ?> <time><?php the_time('F j, Y') ?></time>
						</p>
						<?php the_excerpt(); ?>
						</aside>
						<div class="read-more"><div class="arrow-right"></div><a href="<?php the_permalink(); ?>">Read more</a></div>
					</div>
				</article>
			<?php endforeach; ?>	
			</div>	

			<?php wp_pagenavi(); ?>

		</div><!-- #content -->

	</section><!-- #primary -->

<?php get_footer(); ?>