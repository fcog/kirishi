<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

            <div id="slideshow" class="grid_24">
                <div id="text-slider" class="grid_10">
                    <?php
                    if (is_active_sidebar('homepage-main-text')) :
                        dynamic_sidebar('homepage-main-text');
                    endif;
                    ?>
                </div>
                <div id="image" class="grid_24">
                    <img src="<?php echo get_template_directory_uri(); ?>/photos/img-slider1.png">
                </div>
            </div>
        </div>
    </header>  

    <div id="container" class="container_24">
        <aside class="grid_6">
            <div class="red-box">
                <div class="top-box"></div>
                <div class="content-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/photos/logo-aside.png">
                    <?php $page_data = get_page( 6 ) ?>
                    <h2><?php echo $page_data->post_title ?></h2>
                    <p><?php echo $page_data->post_excerpt ?></p>
                    <div class="link">  
                        <a href="<?php echo get_permalink( $page_data->ID ); ?>">Read More</a>
                    </div>
                </div>
                <div class="bottom-box"></div>
            </div>  
        </aside>
        <section id="founders" class="grid_8 prefix_1">
            <div class="title">
                <h2>Founders</h2>
            </div>
            <article>
                <div class="image grid_3 alpha">
                    <?php $page_data = get_page( 20 ) ?>
                    <?php if (has_post_thumbnail( $page_data->ID ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $page_data->ID ), 'homepage-portrait' ); ?>
                        <img src="<?php echo $image[0]; ?>">
                    <?php endif ?>
                </div>
                <div class="text grid_5 omega">
                    <h2><a href="<?php echo get_permalink( $page_data->ID ); ?>"><?php echo $page_data->post_title ?></a></h2>
                    <p><?php echo $page_data->post_excerpt ?></p>
                    <div class="link">  
                        <a href="<?php echo get_permalink( $page_data->ID ); ?>">Read More</a>
                    </div>
                </div>
            </article>
            <article class="border-none">
                <div class="image grid_3 alpha">
                    <?php $page_data = get_page( 22 ) ?>
                    <?php if (has_post_thumbnail( $page_data->ID ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $page_data->ID ), 'homepage-portrait' ); ?>
                        <img src="<?php echo $image[0]; ?>">
                    <?php endif ?>
                </div>
                <div class="text grid_5 omega">
                    <h2><a href="<?php echo get_permalink( $page_data->ID ); ?>"><?php echo $page_data->post_title ?></a></h2>
                    <p><?php echo $page_data->post_excerpt ?></p>
                    <div class="link">  
                        <a href="<?php echo get_permalink( $page_data->ID ); ?>">Read More</a>
                    </div>
                </div>
            </article>
        </section>
        <section id="blog" class="grid_8 prefix_1">
            <div class="title">
                <h2>Blog</h2>
            </div>
            <?php 
            // Query Arguments
            $args = array(
                'post_type' => array('post'),
                'posts_per_page' => 4,
            );  
     
            // The Query
            $the_query = new WP_Query( $args );

            while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>
                <article>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <p><time> <?php the_time('F d-Y/ G:i'); ?> <time></p>
                </article>
            <?php endwhile ?>   
            <div class="link">  
                <a href="blog">Read More</a>
            </div>
        </section>
    </div>

<?php get_footer(); ?>    