<?php

function mytheme_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'mytheme' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'mytheme' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true );
    add_image_size( 'homepage-portrait', 108, 115, true ); 
  // add_image_size( 'homepage-slider', 480, 323, true ); 
  // add_image_size( 'homepage-multimedia', 189, 189, true ); 
}
add_action( 'after_setup_theme', 'mytheme_setup' );

// function add_class_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'primary' )
//   		$output = preg_replace('/class="menu-item/', 'class="col span_1_5 menu-item', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_class_to_menu_items', 10, 2);

// function add_div_to_menu_items($output, $args) {
// 	if( $args->theme_location == 'footer' )
//   		$output = preg_replace('/<a href/', '<div class="arrow-right"></div><a href', $output);
//   return $output;
// }
// add_filter('wp_nav_menu', 'add_div_to_menu_items', 10, 2);

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function mytheme_widgets_init() {

    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'mytheme' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'homepage-main-text',
      'name'          => __( 'Homepage - Photo Text', 'mytheme' ),
      'before_widget'  => '',                  
    ) );

    // register_sidebar( array(
    //   'id'            => 'homepage-quote',
    //   'name'          => __( 'Homepage - Quote', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) );
 
    // register_sidebar( array(
    //   'id'            => 'footer-slogan',
    //   'name'          => __( 'Footer - Slogan', 'mytheme' ),
    //   'before_widget'  => '',                  
    // ) ); 

    register_sidebar( array(
         'id'            => 'footer-contact-us',
         'name'          => __( 'Footer - Contact Us', 'mytheme' ),
         'before_widget'  => '',                  
     ) );      

    register_sidebar( array(
        'id'            => 'footer-copyright',
        'name'          => __( 'Footer - Copyright', 'mytheme' ),
        'before_widget'  => '',                  
    ) );       

    register_sidebar( array(
      'id'            => 'sidebar-blog',
      'name'          => __( 'Sidebar - Blog', 'mytheme' ),
      'before_widget'  => '',                  
    ) );      

    register_sidebar( array(
      'id'            => 'newsroom-multimedia',
      'name'          => __( 'Newsroom Multimedia', 'mytheme' ),
      'before_widget'  => '',                  
    ) );  
}
add_action( 'widgets_init', 'mytheme_widgets_init' );

/************* CUSTOM POST TYPE *****************/

add_action( 'init', 'register_cpt_news' );

function register_cpt_news() {

    $labels = array( 
        'name' => _x( 'News', 'news' ),
        'singular_name' => _x( 'News', 'news' ),
        'add_new' => _x( 'Add New', 'news' ),
        'add_new_item' => _x( 'Add News', 'news' ),
        'edit_item' => _x( 'Edit News', 'news' ),
        'new_item' => _x( 'New News', 'news' ),
        'view_item' => _x( 'View News', 'news' ),
        'search_items' => _x( 'Search News', 'news' ),
        'not_found' => _x( 'No news found', 'news' ),
        'not_found_in_trash' => _x( 'No news found in Trash', 'news' ),
        'parent_item_colon' => _x( 'Parent News:', 'news' ),
        'menu_name' => _x( 'News', 'news' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Newsroom Posts',
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions' ),
        'taxonomies' => array( 'category', 'post_tag' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'news', $args );
}
/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* EMAIl LINKS *****************/

class EmailLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Email Link','description=Email Link');
        }

        public function form( $instance ) {
            $instance = wp_parse_args( (array) $instance, array( 'link' => '' ) );
                $link = $instance['link'];
            ?>
              <p><label for="<?php echo $this->get_field_id('link'); ?>">Email: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
            <?php
        }

        public function update( $new_instance, $old_instance ) {
            $instance = $old_instance;
            $instance['link'] = $new_instance['link'];
            return $instance;
        }

        public function widget( $args, $instance ) {
            extract($args, EXTR_SKIP);
         
            echo $before_widget;
            $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

            if (!empty($link))
              echo "<li><a href='mailto:".$link."' target='_blank'>".$link."</a></li>";

            echo $after_widget;
        }

}
register_widget( 'EmailLinkWidget' );

/************* HELPER FUNCTIONS *****************/

//Remove menu div container
function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');

// function html_widget_title( $title ) {
// 	//HTML tag opening/closing brackets
// 	$title = str_replace( '[', '<', $title );
// 	$title = str_replace( ']', '/>', $title );

// 	return $title;
// }
// add_filter( 'widget_title', 'html_widget_title' );

// function html_widget_text( $text ) {
// 	//HTML tag opening/closing brackets
// 	$text = str_replace( '[', '<', $text );
// 	$text = str_replace( ']', '/>', $text );

// 	return $text;
// }
// add_filter( 'widget_text', 'html_widget_text' );

