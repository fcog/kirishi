
    <footer id="footer">
        <div class="container_24">
            <div class="menu grid_8">
                 <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'menu-footer' ) ); ?>
            </div>
            <div class="social-network-footer grid_6 prefix_1">
                <ul>
                    Follow us on
                    <?php
                    if (is_active_sidebar('homepage-social-buttons')) :
                        dynamic_sidebar('homepage-social-buttons');
                    endif;
                    ?>
                </ul>
            </div>
            <div class="mail grid_8 prefix_1">
                <span class="grid_2 alpha">Email</span>
                <ul class="grid_6 omega">
                    <?php
                    if (is_active_sidebar('footer-contact-us')) :
                        dynamic_sidebar('footer-contact-us');
                    endif;
                    ?>
                </ul>
            </div>
            <div class="copyright grid_24">
            <?php
            if (is_active_sidebar('footer-copyright')) :
                dynamic_sidebar('footer-copyright');
            endif;
            ?> 
            </div>
        </div>
    </footer>
</div>
<?php wp_footer() ?>
</body>
</html>